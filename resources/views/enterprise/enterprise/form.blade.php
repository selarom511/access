<div class="form-group {{ $errors->has('nombre_empresa') ? 'has-error' : ''}}">
    <label for="nombre_empresa" class="col-md-4 control-label">{{ 'Nombre Empresa' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="nombre_empresa" type="text" id="nombre_empresa" value="{{ $enterprise->nombre_empresa or ''}}" required>
        {!! $errors->first('nombre_empresa', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('nombre_contacto') ? 'has-error' : ''}}">
    <label for="nombre_contacto" class="col-md-4 control-label">{{ 'Nombre Contacto' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="name" type="text" id="nombre_contacto" value="{{ $enterprise->nombre_contacto or ''}}" required>
        {!! $errors->first('nombre_contacto', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('telefono_contacto') ? 'has-error' : ''}}">
    <label for="telefono_contacto" class="col-md-4 control-label">{{ 'Telefono Contacto' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="phone" type="text" id="telefono_contacto" value="{{ $enterprise->telefono_contacto or ''}}" required>
        {!! $errors->first('telefono_contacto', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="col-md-4 control-label">{{ 'Email' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="email" type="email" id="email" value="{{ $enterprise->email or ''}}" required>
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="col-md-4 control-label">{{ 'Password' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="password" type="password" id="password" value="{{ $enterprise->password or ''}}" >
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
