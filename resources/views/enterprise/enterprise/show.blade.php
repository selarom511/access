@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Enterprise {{ $enterprise->id }}</h3>
                    @can('view-'.str_slug('Enterprise'))
                        <a class="btn btn-success pull-right" href="{{ url('/enterprise') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $enterprise->id }}</td>
                            </tr>
                            <tr><th> Nombre Empresa </th><td> {{ $enterprise->nombre_empresa }} </td></tr><tr><th> Nombre Contacto </th><td> {{ $enterprise->nombre_contacto }} </td></tr><tr><th> Telefono Contacto </th><td> {{ $enterprise->telefono_contacto }} </td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

