<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\AddUser;

class Enterprise extends Model
{
    use SoftDeletes;
    use AddUser;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'enterprises';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre_empresa', 'nombre_contacto', 'telefono_contacto', 'email', 'password'];


}
