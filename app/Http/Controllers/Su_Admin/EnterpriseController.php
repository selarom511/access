<?php

namespace App\Http\Controllers\Su_Admin;

use App\Enterprise;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\AddUser;

use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class EnterpriseController extends Controller
{
use AddUser;
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('enterprise','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $enterprise = Enterprise::where('nombre_empresa', 'LIKE', "%$keyword%")
                ->orWhere('nombre_contacto', 'LIKE', "%$keyword%")
                ->orWhere('telefono_contacto', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('password', 'LIKE', "%$keyword%")
                ->paginate($perPage);
            } else {
                $enterprise = Enterprise::paginate($perPage);
            }

            return view('enterprise.enterprise.index', compact('enterprise'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = str_slug('enterprise','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('enterprise.enterprise.create');
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('enterprise','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            $this->validate($request, [
			'nombre_empresa' => 'required',
			'name' => 'required',
			'phone' => 'required',
			'email' => 'required',
			'password' => 'required|min:6'
		]);


 $empresa =  Enterprise::firstOrCreate(['nombre_empresa'=>$request->nombre_empresa]);
    $empresa->save();
 $id_empresa_agregada = $empresa->id;
$algo = $this->createuser($request,$id_empresa_agregada);
if($algo){
}
Session::flash('message','Enterprise has been added');
return redirect('enterprise');

        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('enterprise','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $enterprise = Enterprise::findOrFail($id);
            return view('enterprise.enterprise.show', compact('enterprise'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $model = str_slug('enterprise','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $enterprise = Enterprise::findOrFail($id);
            return view('enterprise.enterprise.edit', compact('enterprise'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('enterprise','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $this->validate($request, [
			'nombre_empresa' => 'required',
			'nombre_contacto' => 'required',
			'telefono_contacto' => 'required',
			'email' => 'required'
		]);
            $requestData = $request->all();

            $enterprise = Enterprise::findOrFail($id);
             $enterprise->update($requestData);

             return redirect('enterprise')->with('flash_message', 'Enterprise updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('enterprise','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            Enterprise::destroy($id);

            return redirect('enterprise')->with('flash_message', 'Enterprise deleted!');
        }
        return response(view('403'), 403);

    }
}
