<?php

namespace App;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
//use Laravel\Socialite\Contracts\User;


trait AddUser
{


    public function createuser($request, $id_empresa_agregada){

$user =  User::firstOrCreate(['name'=>$request->name,'email'=> $request->email]);
       $user->status = 1;
       $user->password = bcrypt($request->password);
       $user->empresa_id = $id_empresa_agregada;
       $user->save();

  if($user->profile == null){
            $profile = new \App\Profile();
            $profile->user_id = $user->id;
            $profile->phone = $request->phone;
            $profile->save();
        }
        $user->assignRole('su_user');
return $user;
    }


}
