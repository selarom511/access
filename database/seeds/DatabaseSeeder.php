<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	  $this->call(PermissionsTableSeeder::class);
         $this->call(AdminSeeder::class);
         $this->call(Permissions_RoleTableSeeder::class);
    }
}
