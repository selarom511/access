<?php

use Faker\Factory;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\Permission;

class AdminSeeder extends DatabaseSeeder
{

    public function run()
    {

        $su_admin = User::firstOrCreate(array(
            'email' => 'su_admin@suadmin.com',
            'name' => 'su_admin'
        ));
        $su_admin->password = bcrypt("password123");
        $su_admin->save();

        if($su_admin->profile == null){
            $profile = new \App\Profile();
            $profile->user_id = $su_admin->id;
            $profile->save();
        }
/*
        $user = User::firstOrCreate(array(
            'email' => 'user@user.com',
            'name' => 'User'
        ));
        $user->password = bcrypt("password123");
        $user->save();

        if($user->profile == null){
            $profile = new \App\Profile();
            $profile->user_id = $user->id;
            $profile->save();
        }
*/
        $admin_role = Role::firstOrcreate(['name' => 'su_admin']);
       // $user_role = Role::firstOrcreate(['name' => 'user']);
        //$permission = Permission::firstOrcreate(['name' => 'All Permission']);
        $su_admin->assignRole('su_admin');
        //$admin_role->assignPermission('All Permission');
       // $user->assignRole('user');

        $this->command->info('Su_Admin created with username su_admin@suadmin.com and password password123');
    }

}