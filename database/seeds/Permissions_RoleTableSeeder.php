<?php

use Illuminate\Database\Seeder;
use\app\Role;
use\app\Permission;
class Permissions_RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::findOrfail(1);
       $permissions = Permission::where('id', '<', 6)->get();
       $role->permissions()->sync($permissions);
    }
}
